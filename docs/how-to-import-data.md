# How to import data

- Login to gitlab.com

- Go to the WebIDE: https://gitlab.com/-/ide/project/agora_team/delas-map/edit/import-delas/-/
    - Check that "import-delas" is the selected branch

![Web IDE button](./img/header.png)

- To edit content enter assets folders:
    - To edit import data (walls info) go to data folder
    - To edit static texts go to locales

![Data folder](./img/IDE-data-folder.png)

- Edit the desired files (remember to change the files for all the languages)

- Commit the file to save your changes
    - Enter a commit name that descrive the changes

![Commit button](./img/commit-button.png)

- Wait until build job finishes:
    - You can check jobs here: https://gitlab.com/agora_team/delas-map/-/jobs

![Jobs](./img/jobs.png)

- To see the results go to: http://delas.lyrainnovation.com/

- You can get the zip with the last version of the application here: http://delas.lyrainnovation.com/delas-map.zip