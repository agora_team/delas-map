#!/bin/bash

BRANCH=$1

ssh developer@delas.lyrainnovation.com << EOF
cd /home/developer/delas-map
git checkout $BRANCH
git pull
npm run build
rm dist/delas-map.zip
zip -r delas-map.zip dist
mv delas-map.zip dist/delas-map.zip
EOF
