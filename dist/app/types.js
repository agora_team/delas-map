export var Categories;
(function (Categories) {
    Categories["marine"] = "marine";
    Categories["terrestrial"] = "terrestrial";
    Categories["virtual"] = "virtual";
    Categories["schengen"] = "schengen";
    Categories["frontex"] = "frontex";
})(Categories || (Categories = {}));
