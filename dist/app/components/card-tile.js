var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, customElement, property, css } from 'lit-element';
let CardTile = class CardTile extends LitElement {
    constructor() {
        super(...arguments);
        this.indicatorColor = '#000000';
        this.indicatorPosition = 'top';
        this.small = false;
        this.triangleIndicator = false;
    }
    static get styles() {
        return css `
      .card {
        background-color: white;
        box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
        position: relative;
        height:100%
      }
      .card-small {
        width: 320px;
      }
      .card-separator {
        height: 8px;
        width: 100%;
      }

      .triangle-indicator {
        position: absolute;
        bottom: 0;
        right: -16px;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 12px 0 12px 16px;
        border-color: transparent transparent transparent #ffffff;
      }
    `;
    }
    render() {
        return html `
      <div class="card ${this.small ? 'card-small' : ''}">
        ${this.indicatorPosition === 'top'
            ? html `
              <div
                class="card-separator"
                style="background-color: ${this.indicatorColor};"
              ></div>
              <div class="card-content" style="padding: 16px;">
                <slot></slot>
              </div>
            `
            : html ``}
        ${this.indicatorPosition === 'bottom'
            ? html `
          <div class="card-content" style="padding-top: 8px; padding-bottom:8px; padding-left:5px;">
            <slot></slot>
          </div>
              <div
                class="card-separator"
                style="background-color: ${this.indicatorColor}"
              ></div>
            `
            : html ``}
      </div>
      ${this.triangleIndicator
            ? html `
            <div class="triangle-indicator"></div>
          `
            : html ``}
    `;
    }
};
__decorate([
    property({ type: String })
], CardTile.prototype, "indicatorColor", void 0);
__decorate([
    property({ type: String })
], CardTile.prototype, "indicatorPosition", void 0);
__decorate([
    property({ type: Boolean })
], CardTile.prototype, "small", void 0);
__decorate([
    property({ type: Boolean })
], CardTile.prototype, "triangleIndicator", void 0);
CardTile = __decorate([
    customElement('card-tile')
], CardTile);
export { CardTile };
