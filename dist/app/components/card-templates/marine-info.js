var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { html, customElement, property } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { sharedStyles } from '../../../shared-styles';
import { get } from '@appnest/lit-translate';
import { Info } from './info';
let MarineInfo = class MarineInfo extends Info {
    constructor() {
        super(...arguments);
        this.color = '#367e8d';
    }
    render() {
        return html `
      ${sharedStyles}

      <div class="card">
        ${super.renderYear()} ${super.renderTitle()}

        <span>${unsafeHTML(get(this.wall.subtitle))}</span>

        <br /><br />
        <span class="gray">
          ${unsafeHTML(get('map.card.terrestrial.geoInfo'))}
        </span>
        <br />
        <span>
          ${unsafeHTML(get(this.wall.geoInfo))}
        </span>
        ${this.renderDescription()}
      </div>
    `;
    }
};
__decorate([
    property({ type: Object })
], MarineInfo.prototype, "wall", void 0);
__decorate([
    property({ type: String })
], MarineInfo.prototype, "color", void 0);
MarineInfo = __decorate([
    customElement('marine-info')
], MarineInfo);
export { MarineInfo };
