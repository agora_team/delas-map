var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, css, property } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { get } from '@appnest/lit-translate';
export class Info extends LitElement {
    constructor() {
        super(...arguments);
        this.color = '#000000';
    }
    static get styles() {
        return css `
      .gray {
        color: gray;
      }
      .title {
        font-weight: bold;
        font-size: 13px;
      }
      .card {
        margin: 10px 20px 10px 10px;
      }
      
      .description {
        font-size: 12px;
      }
      @media screen and (max-width: 1024px) {
        
        .title{
          font-size:13px;
        }

        .description{
          font-size: 12px;
        }
        
        }
        
      @media screen and (max-width: 950px) {
        .title{
          font-size:13px;
        }

        .description{
          font-size: 11px;
        }
        
      }
        
        
      /*@media screen and (max-width: 650px) {
        .title{
          font-size:12px;
        }

        .description{
          font-size: 11px;
        }
      
      }*/
    `;
    }
    renderYear() {
        let startYear = this.yearFromTimestamp(this.wall.startDate);
        let endYear = this.yearFromTimestamp(this.wall.endDate);
        let endYearString = (startYear !== endYear &&
            ' - ' + (endYear ? endYear : get('map.card.current')));
        return html `
      <span class="gray">
        ${unsafeHTML(startYear + (endYearString ? endYearString : ""))}
      </span>
      <br />
    `;
    }
    renderTitle() {
        return html `
      <span class="title" style="color: ${this.color}">
        ${unsafeHTML(get(this.wall.name).toUpperCase())}
      </span>
    `;
    }
    renderDescription() {
        return html `
      ${this.wall.description.map(des => html `
          <p class="description not-justified">${unsafeHTML(get(des))}</p>
        `)}
    `;
    }
    yearFromTimestamp(date) {
        if (!date)
            return null;
        return new Date(date).getFullYear().toString();
    }
}
__decorate([
    property({ type: Object })
], Info.prototype, "wall", void 0);
__decorate([
    property({ type: String })
], Info.prototype, "color", void 0);
