var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, customElement, property } from 'lit-element';
import { translate } from '@appnest/lit-translate';
import '../../card-tile';
import '../../custom-checkbox';
import { Categories } from '../../../types';
let CategoryCheckbox = class CategoryCheckbox extends LitElement {
    constructor() {
        super(...arguments);
        this.checked = true;
        this.checkedImages = {
            [Categories.marine]: 'assets/img/filtreMM.png',
            [Categories.terrestrial]: 'assets/img/filtreMT.png',
            [Categories.schengen]: 'assets/img/filtreES.png',
            [Categories.virtual]: 'assets/img/filtreMV.png',
            [Categories.frontex]: 'assets/img/filtreMMe.png'
        };
    }
    render() {
        return html `
      <card-tile
        indicatorPosition="bottom"
        indicatorColor=${this.color}
        class="category-label"
        style="height: 50px;"
      >
        <custom-checkbox
          checkedImage=${this.checkedImages[this.category]}
          uncheckedImage="assets/img/filtre.png"
          @value-changed=${e => this.categoryClicked(e.detail.value)}
          label=${translate(this.category)}
        ></custom-checkbox>
      </card-tile>
    `;
    }
    categoryClicked(checked) {
        this.dispatchEvent(new CustomEvent('category-checked', {
            detail: {
                checked: checked
            }
        }));
    }
};
__decorate([
    property({ type: String })
], CategoryCheckbox.prototype, "category", void 0);
__decorate([
    property({ type: String })
], CategoryCheckbox.prototype, "color", void 0);
__decorate([
    property({ type: Boolean })
], CategoryCheckbox.prototype, "checked", void 0);
CategoryCheckbox = __decorate([
    customElement('category-checkbox')
], CategoryCheckbox);
export { CategoryCheckbox };
