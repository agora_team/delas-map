var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, customElement, property, css } from 'lit-element';
import { get } from '@appnest/lit-translate';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { sharedStyles } from '../../../../shared-styles';
let TimelineSlider = class TimelineSlider extends LitElement {
    constructor() {
        super(...arguments);
        this.time = 0;
        this.startDate = 315532800000;
        this.endDate = Date.now();
        this.deltaScroll = 365 * 24 * 60 * 60 * 1000;
        this.containerBackground = '#F7F7F7';
        this.timesliderBackground = '#535d64';
        this.yearsInfo = {};
        this.moving = false;
        this.hoverOffset = null;
        this.detailTopPadding = 15;
        this.detailBottomPadding = 200;
    }
    static get styles() {
        return css `
      #container {
        height: 100%;
        width: 20px;
        padding: 0;
        cursor: pointer;
      }

      #slider {
        width: 100%;
        position: relative;
      }

      .year-details {
        pointer-events: none;
        position: absolute;
        left: 32px;
        bottom: -12px;
      }

      .year-label {
        font-size: 30px;
        font-weight: bold;
      }

      .year-info {
        position: absolute;
        top: 36px;
        width: 230px;

        font-size: 18px;
        font-weight: 100;
      }

      .hover-container {
        position: absolute;
        right: -42px;
        background: url('assets/img/scroll bubble.png');
        width: 41px;
        height: 20px;
        display: flex;
        align-items: center;
        place-content: center;
      }

      .hover-label {
        font-size: 12px;
        color: white;
        font-weight: bold;
        margin-left: 4px;
      }

      @media screen and (max-width: 1024px) {
        .year-label{
          font-size: 28px;
        }

        .year-info{
          font-size: 16px;
        }

        .hover-label{
          font-size:11px;
        }
        
      }

      @media screen and (max-width: 950px) {
        .year-label{
          font-size: 24px;
        }

        .year-info{
          font-size: 14px;
        }

        .hover-label{
          font-size:11px;
        }
       
      }
      
      /*@media screen and (max-width: 650px) {
        .year-label{
          font-size: 18px;
        }

        .year-info{
          font-size: 12px;
        }

        .hover-label{
          font-size:11px;
        }
       
       
      }*/
       
    `;
    }
    render() {
        return html `
      ${sharedStyles}
      <style>
        #container {
          background: ${this.containerBackground};
        }

        #slider {
          background: ${this.timesliderBackground};
        }
      </style>
      <div
        id="container"
        class="ol-timeline ol-unselectable ol-control"
        @mousedown=${e => {
            this.moving = true;
            this.setTimeByOffset(e.offsetY);
        }}
        @mouseout=${e => (this.hoverOffset = null)}
        @mousemove=${e => this.onContainerHover(e)}
      >
        <div
          id="slider"
          style="height: ${this.timeToHeight(this.time)}px; ${new Date(this.time).getFullYear().toString() == '2016' &&
            this.getContainerHeight() - this.timeToHeight(this.time) <=
                this.detailBottomPadding + 20 && this.time > 0
            ? 'position: static;'
            : ''}"
        >
          <div
            class="year-details column"
            style="top: ${this.timeToHeight(this.time) <=
            this.detailTopPadding + 20
            ? this.detailTopPadding + 'px'
            : 'unset'};
                ${new Date(this.time).getFullYear().toString() == '2016' &&
            this.getContainerHeight() - this.timeToHeight(this.time) <=
                this.detailBottomPadding + 20 && this.time > 0
            ? 'bottom: ' + this.detailBottomPadding + 'px;'
            : ''}"
          >
            <span class="year-label">
              ${new Date(this.time).getFullYear().toString()}
            </span>
            ${this.yearsInfo[new Date(this.time).getFullYear()]
            ? html `
                  <span class="year-info">
                    ${unsafeHTML(get(this.yearsInfo[new Date(this.time).getFullYear()]))}
                  </span>
                `
            : html ``}
          </div>
        </div>

        ${this.hoverOffset && !this.moving
            ? html `
              <div class="hover-container" style="top: ${this.hoverOffset}px;">
                <span class="hover-label">
                  ${this.offsetToYear(this.hoverOffset)}
                </span>
              </div>
            `
            : html ``}
      </div>
    `;
    }
    onContainerHover(event) {
        if (Math.abs(event.offsetY - this.timeToHeight(this.time)) > 40) {
            this.hoverOffset = event.offsetY;
        }
        else {
            this.hoverOffset = null;
        }
    }
    firstUpdated() {
        this.ownerDocument.addEventListener('mouseup', () => (this.moving = false));
        this.ownerDocument.addEventListener('mousemove', event => {
            if (this.moving) {
                this.setTimeByOffset(event.offsetY);
            }
        });
        this.ownerDocument.addEventListener('wheel', event => {
            if (this.getBoundingClientRect().top === 0 &&
                !(this.time === this.startDate && event.deltaY < 0)) {
                this.setTime(event.deltaY > 0
                    ? this.time + this.deltaScroll
                    : this.time - this.deltaScroll);
                event.preventDefault();
            }
        }, { passive: false });
        this.setTime(this.time);
    }
    getContainerHeight() {
        const container = this.shadowRoot.getElementById('container');
        return container ? container.clientHeight : 1;
    }
    /**
     * Converts the given time in ms to the slider's height in px
     */
    timeToHeight(time) {
        return (((time - this.startDate) * this.getContainerHeight()) /
            (this.endDate - this.startDate));
    }
    setTime(time) {
        if (time < this.startDate) {
            time = this.startDate;
        }
        else if (time > this.endDate) {
            time = this.endDate;
        }
        this.time = time;
        this.dispatchEvent(new CustomEvent('time-updated', {
            detail: {
                time: time
            }
        }));
    }
    offsetToTime(offsetY) {
        return (this.startDate +
            (offsetY * (this.endDate - this.startDate)) / this.getContainerHeight());
    }
    offsetToYear(offsetY) {
        return new Date(this.offsetToTime(offsetY)).getFullYear().toString();
    }
    setTimeByOffset(offsetY) {
        this.setTime(this.offsetToTime(offsetY));
    }
};
__decorate([
    property({ type: Number })
], TimelineSlider.prototype, "time", void 0);
__decorate([
    property({ type: Number })
], TimelineSlider.prototype, "startDate", void 0);
__decorate([
    property({ type: Number })
], TimelineSlider.prototype, "endDate", void 0);
__decorate([
    property({ type: Number })
], TimelineSlider.prototype, "deltaScroll", void 0);
__decorate([
    property({ type: String })
], TimelineSlider.prototype, "containerBackground", void 0);
__decorate([
    property({ type: String })
], TimelineSlider.prototype, "timesliderBackground", void 0);
__decorate([
    property({ type: Object })
], TimelineSlider.prototype, "yearsInfo", void 0);
__decorate([
    property({ type: Boolean })
], TimelineSlider.prototype, "moving", void 0);
__decorate([
    property({ type: Number })
], TimelineSlider.prototype, "hoverOffset", void 0);
__decorate([
    property({ type: Number })
], TimelineSlider.prototype, "detailTopPadding", void 0);
__decorate([
    property({ type: Number })
], TimelineSlider.prototype, "detailBottomPadding", void 0);
TimelineSlider = __decorate([
    customElement('timeline-slider')
], TimelineSlider);
export { TimelineSlider };
