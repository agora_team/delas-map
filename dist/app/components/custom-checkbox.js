var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, customElement, property, css } from 'lit-element';
import { sharedStyles } from '../../shared-styles';
let CustomCheckbox = class CustomCheckbox extends LitElement {
    constructor() {
        super(...arguments);
        this.checked = true;
    }
    static get styles() {
        return css `
      .container {
        display: flex;
        align-items: center;
      }

      .checkbox {
        display: none;
      }

      .checkbox + label {
        background-position-y: center;
        background-size: 100%;
        height: 16px;
        width: 16px;
        display: inline-block;
        padding: 0 0 1px 0px;
        margin-right: 8px;
      }

      .checkbox:checked + label {
        background-position-y: center;
        background-size: 100%;
        height: 16px;
        width: 16px;
        display: inline-block;
        padding: 0 0 1px 0px;
        margin-right: 8px;
      }
    `;
    }
    render() {
        return html `
      ${sharedStyles}
      <style>
        .checkbox + label {
          background: url(${this.uncheckedImage}) no-repeat;
        }

        .checkbox:checked + label {
          background: url(${this.checkedImage}) no-repeat;
        }
      </style>

      <div class="container">
        <input
          id="checkbox"
          class="checkbox"
          type="checkbox"
          checked=${this.checked}
          @click=${e => this.checkboxClicked()}
        />
        <label for="checkbox"> </label>
        <label for="checkbox"> ${this.label} </label>
      </div>
    `;
    }
    checkboxClicked() {
        this.checked = !this.checked;
        this.dispatchEvent(new CustomEvent('value-changed', {
            detail: { value: this.checked }
        }));
    }
};
__decorate([
    property({ type: Boolean })
], CustomCheckbox.prototype, "checked", void 0);
__decorate([
    property({ type: String })
], CustomCheckbox.prototype, "checkedImage", void 0);
__decorate([
    property({ type: String })
], CustomCheckbox.prototype, "uncheckedImage", void 0);
__decorate([
    property({ type: String })
], CustomCheckbox.prototype, "label", void 0);
CustomCheckbox = __decorate([
    customElement('custom-checkbox')
], CustomCheckbox);
export { CustomCheckbox };
