var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, customElement, property, css } from 'lit-element';
import { Map, View, Feature, Overlay } from 'ol';
import { fromLonLat } from 'ol/proj';
import XYZ from 'ol/source/XYZ';
import VectorLayer from 'ol/layer/Vector.js';
import VectorSource from 'ol/source/Vector.js';
import GeoJSON from 'ol/format/GeoJSON.js';
import { Fill, Stroke, Style, Icon, Circle as CircleStyle } from 'ol/style';
import { Point } from 'ol/geom';
import { Tile as TileLayer } from 'ol/layer';
import { defaults as defaultInteractions, Select } from 'ol/interaction';
import { defaults as defaultControls } from 'ol/control';
import { easeOut } from 'ol/easing';
import { unByKey } from 'ol/Observable';
import { sharedStyles, OVERLAY_COLUMN_WIDTH } from '../../shared-styles';
import { Categories } from '../types';
import { TimelineControl } from './controls/timeline/timeline-control';
import { addCategoryControl } from './controls/category/category-control';
import './card-tile';
import './nav-arrow';
import './card-templates/card-info';
import './card-templates/marine-info';
import './card-templates/terrestrial-info';
import './event-timeline';
let OpenLayersMap = class OpenLayersMap extends LitElement {
    constructor() {
        super(...arguments);
        this.initialLat = 54;
        this.initialLon = 20;
        this.layers = [];
        this.schengenInfo = {};
        /** Private */
        this.time = 0;
    }
    static get styles() {
        return css `
      #ol-map {
        position: relative;
        background-color: rgb(248, 244, 240);
      }

      .ol-zoom {
        top: unset !important;
        left: unset !important;
        right: 0.5em !important;
        bottom: 0.5em !important;
      }

      #popup {
        position: absolute;
        right: 36px;
        bottom: -8px;
      }

      .ol-attribution {
        right: 56px !important;
      }

      timeline-slider {
        top: 0;
        left: 0;
        height: 100%;
        position: absolute;
      }

      nav-arrow {
        position: absolute;
        left: 50%;
        transform: translate(-50%);
        top: 16px;
        z-index: 10;
      }

      .tile {
        font-size: 12px;
      }

      .static {
        position: static !important;
      }

      .timeline-walls-overlay {
        position: absolute;
        top: 0 !important;
        left: auto !important;
      }

      .close-button {
        position: absolute;
        top: 18px;
        right: 12px;
        cursor: pointer;
        background-color: unset;
        border-style: none;
        font-size: 24px;
        color: grey;
      }

      .ol-control button {
        color: black;
        background-color: white;
        border: 1px solid black;
        margin: 0;
      }
      .ol-control button:hover,
      .ol-control button:focus {
        background-color: #cecece;
      }
      .ol-zoom-in {
        border-bottom: 0 !important;
      }

      @media screen and (max-width: 1024px) {
        .title{
          font-size:11px;
        }

        .close-button{
          font-size:22px;
        }        
      }
      
      @media screen and (max-width: 950px) {
        .title{
          font-size:10px;
        }

        .close-button{
          font-size:18px;
        }

        nav-arrow{
          display:none;
        }
       
      }

    `;
    }
    render() {
        return html `
      ${sharedStyles}
      <link rel="stylesheet" href="node_modules/ol/ol.css" />

      <div tabindex="1" id="ol-map" class="fullscreen">
      </div>
      <div id="popup" class="ol-popup">
        ${this.selectedFeature
            ? html `
              <card-tile
                .small=${true}
                class="tile"
                .indicatorColor=${this.selectedFeature.getProperties().layer
                .color}
                .triangleIndicator=${true}
              >
                ${this.renderFeature(this.selectedFeature)}
                <button class="close-button" @click=${e => this.closePopup()}>
                  &#10799;
                </button>
              </card-tile>
            `
            : html ``}
      </div>
      <div id="timeline-overlay-container">
        ${this.layers
            .filter(layer => layer.timeWalls.length > 0)
            .reverse()
            .map((layer, index) => {
            let distance = window.ismobile ? index * OVERLAY_COLUMN_WIDTH : index * OVERLAY_COLUMN_WIDTH;
            return html `
              <event-timeline
                id="timeline-${layer.category}"
                class="timeline-walls-overlay"
                style="right: ${distance}px;"
                .dark=${layer.dark}
                .backgroundColor=${layer.color}
                .events=${layer.timeWalls}
                .currentTime=${this.time}
              ></event-timeline>
            `;
        })}
      </div>
    `;
    }
    renderFeature(feature) {
        const properties = feature.getProperties();
        if (properties.layer.category === Categories.marine) {
            return html `
        <marine-info .wall=${properties.wall}></marine-info>
      `;
        }
        else if (properties.layer.category === Categories.terrestrial) {
            return html `
        <terrestrial-info .wall=${properties.wall}></terrestrial-info>
      `;
        }
        else {
            return html `
        <card-info
          .color=${properties.layer.color}
          .wall=${properties.wall}
        ></card-info>
      `;
        }
    }
    firstUpdated() {
        this.drawMap();
        this.updateTime(this.time);
    }
    updated(changedProperties) {
        super.updated(changedProperties);
        this.map.render();
    }
    drawMap() {
        let schengenLayer = this.getSchengenLayer();
        let mapboxLayer = this.getMapBoxLayer();
        let terrestrialLayer = this.getTerrestrialLayer();
        /**
         * Create an overlay to anchor the popup to the map.
         */
        this.overlay = new Overlay({
            element: this.shadowRoot.getElementById('popup'),
            autoPan: true,
            autoPanAnimation: {
                duration: 250
            },
            stopEvent: false
        });
        // Map declaration
        const center = fromLonLat([this.initialLon, this.initialLat]);
        this.map = new Map({
            interactions: defaultInteractions({
                mouseWheelZoom: false
            }),
            controls: defaultControls().extend([
                new TimelineControl(this.shadowRoot, time => this.updateTime(time), {
                    yearsInfo: this.yearsInfo
                })
            ]),
            target: this.shadowRoot.getElementById('ol-map'),
            layers: [mapboxLayer, schengenLayer, terrestrialLayer],
            overlays: [this.overlay],
            view: new View({
                center: center,
                zoom: 4,
                minZoom: 3,
                maxZoom: 6,
                extent: [
                    center[0] - 3000000,
                    center[1] - 3000000,
                    center[0] + 3000000,
                    center[1] + 5000000
                ]
            })
        });
        const selectableLayers = [];
        this.layers.forEach(layer => {
            const objectsToHide = [];
            if (layer.geoWalls.length > 0) {
                const markerLayer = this.buildMarkersLayer(layer);
                objectsToHide.push(markerLayer);
                selectableLayers.push(markerLayer);
            }
            if (layer.timeWalls.length > 0) {
                const timelineLayer = this.buildTimelineLayer(layer);
                objectsToHide.push(timelineLayer);
            }
            if (layer.category === Categories.schengen) {
                objectsToHide.push(schengenLayer);
            }
            if (layer.category === Categories.terrestrial) {
                objectsToHide.push(terrestrialLayer);
            }
            if (!layer['disableFilter']) {
                addCategoryControl(this.shadowRoot, this.map, objectsToHide, layer.category, layer.color);
            }
        });
        this.addSelectInteraction(selectableLayers);
        this.addHoverInteraction();
    }
    addHoverInteraction() {
        let map = this.map;
        this.map.on('pointermove', function (e) {
            var pixel = map.getEventPixel(e.originalEvent);
            var hit = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
                return feature.getProperties().type == 'Marker';
            });
            map.getViewport().style.cursor = hit ? 'pointer' : '';
        });
    }
    addSelectInteraction(layers) {
        this.select = new Select({ layers: layers });
        this.select.on('select', e => {
            this.selectedFeature = e.selected[0];
            if (this.selectedFeature) {
                const coordinates = this.selectedFeature.getProperties()
                    .geometry.flatCoordinates;
                this.overlay.setPosition(coordinates);
                // Animate center map to let the card visible at the center
                const zoom = this.map.getView().getZoom();
                this.map.getView().animate({
                    center: [
                        coordinates[0] - 8000000 / (zoom * zoom),
                        coordinates[1] + 20300000 / (zoom * zoom)
                    ]
                });
            }
        });
        this.map.addInteraction(this.select);
    }
    closePopup() {
        this.overlay.setPosition(undefined);
        this.selectedFeature = undefined;
        this.select.getFeatures().clear();
    }
    /**
     * Iterates through all the features in the map and hides those
     * which given the time is out of range
     */
    updateTime(time) {
        this.time = time;
        if (this.map) {
            this.map.getLayers().forEach(layer => {
                if ('featuresRtree_' in layer.getSource()) {
                    layer.getSource().forEachFeature(feature => {
                        const properties = feature.getProperties();
                        let endDate = (properties.type == 'Marker' && properties.layer.category == Categories.marine) ? properties.endDate : new Date().valueOf();
                        const showFeature = properties.startDate <= time &&
                            (!properties.endDate || endDate >= time);
                        if (!feature.getStyle()) {
                            feature.setStyle(new Style());
                        }
                        if (showFeature && properties.type == 'Schengen') {
                            feature.setStyle(new Style({
                                fill: new Fill({
                                    color: 'rgba(0, 0, 0, 0.15)'
                                }),
                                stroke: new Stroke({
                                    color: '#A2A2A2',
                                    width: 1
                                })
                            }));
                        }
                        if (showFeature && properties.type == 'Terrestrial') {
                            feature.setStyle(new Style({
                                stroke: new Stroke({
                                    color: '#ca0000',
                                    width: 3
                                })
                            }));
                        }
                        if (showFeature && properties.type === 'Marker') {
                            // this.animateMarker(layer, feature);
                        }
                        const renderer = showFeature ? null : () => null;
                        feature.getStyle().setRenderer(renderer);
                    });
                    layer.changed();
                }
            });
            setTimeout(() => this.map.render());
        }
    }
    getSchengenLayer() {
        var thisClass = this;
        var vectorLayer = new VectorLayer({
            source: new VectorSource({
                url: 'assets/data/countries.json',
                format: new GeoJSON()
            })
        });
        vectorLayer.getSource().on('addfeature', ({ feature }) => {
            let startDateDef = thisClass.schengenInfo[feature.values_.ADMIN];
            let startDate = startDateDef ? startDateDef : Date.now() + 1;
            feature.setProperties({
                startDate: startDate,
                type: 'Schengen'
            });
            feature.setStyle(new Style({ renderer: () => null }));
        });
        vectorLayer.setZIndex(1);
        return vectorLayer;
    }
    getTerrestrialLayer() {
        var vectorLayer = new VectorLayer({
            source: new VectorSource({
                url: 'assets/data/terrestrial_walls.json',
                format: new GeoJSON()
            }),
            style: function (feature) {
                feature.setProperties({
                    type: 'Terrestrial'
                });
                return new Style();
            },
            renderBuffer: 1000
        });
        vectorLayer.setZIndex(2);
        return vectorLayer;
    }
    getMapBoxLayer() {
        return new TileLayer({
            source: new XYZ({
                url: 'https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGF2aWRnbmluIiwiYSI6ImNqOXprbGx2YTVjbGwzM3BobjU1djQxenUifQ.sWvGaIhoIywSwTlqbQwgqQ'
            })
        });
    }
    buildTimelineLayer(layer) {
        const overlay = new Overlay({
            element: this.shadowRoot.getElementById('timeline-' + layer.category),
            className: 'static',
            position: [10, 10],
            stopEvent: false
        });
        this.map.addOverlay(overlay);
        return overlay;
    }
    animateMarker(layer, feature) {
        let properties = feature.getProperties();
        if (!properties.animating && properties.layer.category !== Categories.terrestrial) {
            feature.setProperties(Object.assign(Object.assign({}, feature.getProperties()), { animating: true }));
            setTimeout(() => {
                let interval;
                const flash = () => {
                    if (feature.getStyle().getRenderer() === null && layer.getVisible()) {
                        this.flash(feature, feature.getProperties().layer.color);
                        this.map.render();
                    }
                    else {
                        feature.setProperties(Object.assign(Object.assign({}, feature.getProperties()), { animating: false }));
                        clearInterval(interval);
                    }
                };
                interval = setInterval(flash, 5000);
                flash();
            }, Math.random() * 4000);
        }
    }
    buildMarkersLayer(layer) {
        let vectorLayer;
        const markersFeatures = layer.geoWalls.map(wall => {
            const feature = new Feature({
                geometry: new Point(fromLonLat([wall.longitude, wall.latitude])),
                name: wall.name
            });
            feature.setProperties({
                layer: layer,
                wall: wall,
                startDate: wall.startDate,
                endDate: wall.endDate,
                type: 'Marker'
            });
            feature.setStyle(new Style({
                image: new Icon({
                    scale: 1,
                    src: wall['frontex'] ? layer.frontexImage : layer.image
                })
            }));
            return feature;
        });
        const layerSource = new VectorSource({
            features: markersFeatures
        });
        vectorLayer = new VectorLayer({
            source: layerSource
        });
        vectorLayer.setZIndex(2);
        this.map.addLayer(vectorLayer);
        return vectorLayer;
    }
    flash(feature, hexColor) {
        var duration = 2500;
        var start = new Date().getTime();
        var listenerKey = this.map.on('postcompose', animate);
        var map = this.map;
        function hexToRgb(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result
                ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16)
                }
                : null;
        }
        function animate(event) {
            var vectorContext = event.vectorContext;
            var frameState = event.frameState;
            var flashGeom = feature.getGeometry().clone();
            var elapsed = frameState.time - start;
            var elapsedRatio = elapsed / duration;
            // radius will be 5 at start and 30 at end.
            var radius = easeOut(elapsedRatio) * 15 + 5;
            var opacity = easeOut(0.5 - elapsedRatio / 2);
            var rgb = hexToRgb(hexColor);
            var style = new Style({
                image: new CircleStyle({
                    radius: radius,
                    stroke: new Stroke({
                        color: 'rgba(' +
                            rgb.r +
                            ', ' +
                            rgb.g +
                            ', ' +
                            rgb.b +
                            ', ' +
                            opacity +
                            ')',
                        width: 0.25 + opacity
                    })
                })
            });
            vectorContext.setStyle(style);
            vectorContext.drawGeometry(flashGeom); // tell OpenLayers to continue postcompose animation
            if (elapsed > duration) {
                unByKey(listenerKey);
                return;
            }
            map.render();
        }
    }
};
__decorate([
    property({ type: Number })
], OpenLayersMap.prototype, "initialLat", void 0);
__decorate([
    property({ type: Number })
], OpenLayersMap.prototype, "initialLon", void 0);
__decorate([
    property({ type: Array })
], OpenLayersMap.prototype, "layers", void 0);
__decorate([
    property({ type: Object })
], OpenLayersMap.prototype, "yearsInfo", void 0);
__decorate([
    property({ type: Object })
], OpenLayersMap.prototype, "schengenInfo", void 0);
__decorate([
    property({ type: Number })
], OpenLayersMap.prototype, "time", void 0);
__decorate([
    property({ type: Object })
], OpenLayersMap.prototype, "selectedFeature", void 0);
OpenLayersMap = __decorate([
    customElement('ol-map')
], OpenLayersMap);
export { OpenLayersMap };
