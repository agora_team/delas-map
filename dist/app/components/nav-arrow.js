var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, customElement, property } from 'lit-element';
let NavArrow = class NavArrow extends LitElement {
    constructor() {
        super(...arguments);
        this.direction = 'up';
    }
    images() {
        return {
            up: 'assets/img/up.png',
            down: 'assets/img/down.png'
        };
    }
    render() {
        return html `
      <style>
        .arrow {
        }
        .arrow img {
          height: 32px;
          width: 32px;
        }
        .arrow-up {
          top: 24px;
        }
        .arrow-down {
          bottom: 24px;
        }
      </style>

      <a href=${this.href} class="arrow arrow-${this.direction}">
        <img src="${this.images()[this.direction]}" />
      </a>
    `;
    }
};
__decorate([
    property({ type: String })
], NavArrow.prototype, "direction", void 0);
__decorate([
    property({ type: String })
], NavArrow.prototype, "href", void 0);
NavArrow = __decorate([
    customElement('nav-arrow')
], NavArrow);
export { NavArrow };
