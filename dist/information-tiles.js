var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, customElement, property } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { get } from '@appnest/lit-translate';
import './app/components/card-tile';
import './app/components/nav-arrow';
import { sharedStyles } from './shared-styles';
let InformationTiles = class InformationTiles extends LitElement {
    constructor() {
        super(...arguments);
        this.informationTiles = [
            {
                class: 'schengen',
                color: 'black',
                image: 'assets/img/ES.png',
                title: 'information.schengen.title',
                description: 'information.schengen.description'
            },
            {
                class: 'frontex',
                color: 'black',
                image: 'assets/img/F.png',
                title: 'information.frontex.title',
                description: 'information.frontex.description'
            },
            {
                class: 'terrestrial_walls',
                color: '#ca0000',
                image: 'assets/img/MT.png',
                title: 'information.terrestrial_walls.title',
                description: 'information.terrestrial_walls.description'
            },
            {
                class: 'maritime_walls',
                color: '#367e8d',
                image: 'assets/img/MM.png',
                title: 'information.maritime_walls.title',
                description: 'information.maritime_walls.description'
            },
            {
                class: 'mental_walls',
                color: 'black',
                image: 'assets/img/MMe.png',
                title: 'information.mental_walls.title',
                description: 'information.mental_walls.description'
            },
            {
                class: 'virtual_walls',
                color: '#5f5146',
                image: 'assets/img/MV.png',
                title: 'information.virtual_walls.title',
                description: 'information.virtual_walls.description'
            }
        ];
    }
    renderTile(tile) {
        return html `
      <card-tile indicatorColor="${tile.color}">
        <div class="row ${tile.class}">
          <img class="icon" src="${tile.image}" />
          <span class="tile-title" style="color: ${tile.color}">
            ${unsafeHTML(get(tile.title).toUpperCase())}
          </span>
        </div>

        <p class="tile-text">${unsafeHTML(get(tile.description))}</p>
      </card-tile>
    `;
    }
    render() {
        return html `
      ${sharedStyles}
      <style>
        .container {
          background-color: #e7e7e7;
          display: flex;
          justify-content: center;
          padding: 16px;
        }
        .information-panel {
          position: relative;
          flex-basis: 80%;
        }
        .tile-title {
          margin-left: 8px;
          font-size: 18px;
          font-weight: bold;
        }

        .tile-text {
          font-size: 12px !important;
        }

        .tiles-container {
          flex-wrap: wrap;
          justify-content: center;
        }

        .arrow-up {
          margin-bottom: 24px;
        }

        .arrow-down {
          margin-top: 24px;
        }

        .information-text {
          font-size: 14px;
        }

        .schengen img,.frontex img,.mental_walls img,.virtual_walls img {
          height: 18px;
        }

        card-tile {
          margin: 16px;
          flex-basis: 488px;
        }
        
        @media screen and (max-width:1024px) {
          .tile-title{
            font-size:16px;
          }

          .tile-text{
            font-size:11px !important;
          }

          .information-text{
            font-size: 13px;
          }

        }
        @media screen and (max-width: 950px) {
          .tile-title{
            font-size:14px;
          }

          .tile-text{
            font-size:10px !important;
          }

          .information-text{
            font-size: 12px;
          }

          .arrow-down {
            display:none;
          }
          nav-arrow{
            display:none;
          }

          nav-arrow .arrow-down{
            display:none;
          }
         }
        
        @media all and (max-width: 450px) {
          .tiles-container {
            /* On small screens, we are no longer using row direction but column */
            flex-direction: column;
          }
          card-tile {
            flex-basis: unset;
          }

          nav-arrow{
            display:none;
          }
          .arrow-down {
            display:none;
          }
        }

        .p-information-description{
          width: 84%;
          text-align: justify;
        }
        .p-quote{
          width: 75%;
          text-align: center;
        }
      </style>
      <div class="row container">
        <div class="column center-children information-panel">

          <h2>
            ${unsafeHTML(get('information.title'))}
          </h2>
          <div class="p-quote">
            <p>
              ${unsafeHTML(get('information.quote1'))}
            </p>
          </div>
          <div class="p-quote">
            <p>
              ${unsafeHTML(get('information.quote2'))}
            </p>
          </div>
          <div class="p-information-description">
            <p>
              ${unsafeHTML(get('information.description'))}
            </p>
          </div>

          <div class="row tiles-container">
            ${this.informationTiles.map(tile => this.renderTile(tile))}
          </div>

          <p class="information-text">
            ${unsafeHTML(get('information.description2'))}
          </p>

          <nav-arrow
            class="arrow-down"
            direction="down"
            href="#map"
          ></nav-arrow>
        </div>
      </div>
    `;
    }
};
__decorate([
    property({ type: Array })
], InformationTiles.prototype, "informationTiles", void 0);
InformationTiles = __decorate([
    customElement('information-tiles')
], InformationTiles);
export { InformationTiles };
