import { html } from 'lit-element';
export const OVERLAY_COLUMN_WIDTH = 130;
export const sharedStyles = html `
  <style>
    .fullscreen {
      height: 100vh;
      width: 100%;
    }

    .row {
      display: flex;
      flex-direction: row;
    }

    .column {
      display: flex;
      flex-direction: column;
    }

    .center-children {
      align-items: center;
      place-content: center;
    }

    .icon {
      padding-top: 2px;
      height: 15px;
    }

    h1 {
      font-size: 72px;
      font-weight: bold;
      
    }

    h2 {
      font-size: 24px;
      font-weight: bold;
      margin: 0;
      margin-bottom: 12px;
    }

    p:not(.not-justified) {
      font-size: 18px;
      font-weight: 100;
      text-align: justify;
      text-justify: inter-word;
    }

    span,
    label {
      font-size: 12px;
    }

    .overlay-column {
      width: ${OVERLAY_COLUMN_WIDTH}px;
    }

    @media screen and (max-width: 1024px) {
      h1{
        font-size:60px;
      }

      h2{
        font-size: 21px;
      }

      p:not(.not-justified) {
        font-size:16px;
      }
      
    }

    @media screen and (max-width: 950px) {
      h1{
        font-size:48px;
      }

      h2{
        font-size: 18px;
      }

      p:not(.not-justified) {
        font-size:14px;
      }
     
    }

    @media screen and (max-width: 650px) {
      h1{
        font-size:36px;
      }

      h2{
        font-size: 16px;
      }

      p:not(.not-justified) {
        font-size:14px;
      }
     
     
     }
     
     @media screen and (max-width: 480px) {
      h1{
        font-size:30px;
      }

      h2{
        font-size: 14px;
      }

      p:not(.not-justified) {
        font-size:12px;
      }
     
     }
  </style>
`;
