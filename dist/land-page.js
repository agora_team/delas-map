var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, html, customElement, css } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { sharedStyles } from './shared-styles';
import { get } from '@appnest/lit-translate';
import './app/components/nav-arrow';
let LandPage = class LandPage extends LitElement {
    static get styles() {
        return css `
      .delas-icon {
        height: 64px;
        width: 64px;
        position: absolute;
        top: 16px;
        left: 16px;
      }
      
      .landpage {
        background-color: #a6b9c7;
        background-image: url('assets/img/muro.jpg');
        background-position: bottom;
        background-repeat: no-repeat;
        background-size: 100%;
        color: white;
      }

      .landpage-content {
        
        flex-grow: 1;
        z-index: 10;
        width: 84%;
        max-width: 782px;
        margin-top: -12vw;

      }

      .languages {
        position: absolute;
        right: 16px;
        left: 16px;
        top: 8px;
        z-index: 11;
      }

      .language {
        margin-left: 8px;
        margin-top: 8px;
        font-weight: bold;
        background-color: unset;
        color: white;
        border-style: none;
        border-color: unset;
        cursor: pointer;
      }

      .separator {
        margin: 0;
        width: 100%;
        border-style: solid;
        color: white;
      }

      .social-networks {
        font-size: 12px;
        font-weight: bold;
        align-self: flex-end;
        align-items: center;
        color: #626b72;
      }

      .icon {
        margin-left: 8px;
      }

      nav-arrow {
        margin-bottom: 16px;
      }
	  
	  .colaborators {
	  	color: black;
		  position: absolute;
    	bottom: 10px;
    	right: 20px;
	  }
	  
	  .colaborators_logo {
	  	height: 40px;
		  vertical-align: middle;
    	margin-left: 15px;
    }
    
    @media screen and (max-width: 950px) {
      .social-networks{
        font-size:12px;
      }

      .first-arrow-down{
        display:none;
      }
     }
    
    /*@media screen and (max-width: 650px) {
      .social-networks{
        font-size:11px;
      }
    }*/
    `;
    }
    render() {
        return html `
      ${sharedStyles}
      <div
        class="landpage fullscreen column center-children"
        style="position: relative;"
      >
        <div class="languages column">
          <hr class="separator" />
          <div class="row" style="align-self: flex-end;">
            <button @click=${e => {
            window.localStorage.setItem("lang", "ca");
            location.reload();
        }} class="language" title="CATALÀ">
              CAT
            </button>
            <button @click=${e => {
            window.localStorage.setItem("lang", "es");
            location.reload();
        }} class="language" title="ESPAÑOL">
              CAST
            </button>
            <button @click=${e => {
            window.localStorage.setItem("lang", "en");
            location.reload();
        }} class="language" title="ENGLISH">
              EN
            </button>
          </div>
        </div>

        <img
          class="delas-icon"
          src="assets/img/logo_delas.png"
          alt=${get('landpage.delasalticon')}
        />

        <div class="column center-children landpage-content">
          <h1>${unsafeHTML(get('landpage.title'))}</h1>
          <h2>${unsafeHTML(get('landpage.subtitle'))}</h2>
          <p class="description">
            ${unsafeHTML(get('landpage.description'))}
          </p>
        </div>

        <nav-arrow direction="down" href="#information" class="first-arrow-down"></nav-arrow>
		
		<div class="colaborators">
			${get('landpage.colaboration_of')}
			<img src="assets/img/colaborators_bcn.png" class="colaborators_logo">
      <img src="assets/img/logo-vector-amb.png" class="colaborators_logo">
		</div>
      </div>
    `;
    }
};
LandPage = __decorate([
    customElement('land-page')
], LandPage);
export { LandPage };
