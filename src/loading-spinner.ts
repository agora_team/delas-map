import {
  LitElement,
  html,
  customElement,
  css
} from 'lit-element';

@customElement('loading-spinner')
export class LoadingSpinner extends LitElement {
  static get styles() {
    return css`
      .loader {
        border: 12px solid #f3f3f3; /* Light grey */
        border-top: 12px solid #a6b9c7; /* Blue */
        border-radius: 50%;
        width: 80px;
        height: 80px;
        animation: spin 0.8s linear infinite;
      }

      @keyframes spin {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }
    `;
  }

  render() {
    return html`
      <div class="loader"></div>
    `;
  }
}
