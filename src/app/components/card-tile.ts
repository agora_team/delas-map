import { LitElement, html, customElement, property, css } from 'lit-element';

@customElement('card-tile')
export class CardTile extends LitElement {
  @property({ type: String })
  public indicatorColor: string = '#000000';

  @property({ type: String })
  public indicatorPosition: 'top' | 'bottom' = 'top';

  @property({ type: Boolean })
  public small: boolean = false;

  @property({ type: Boolean })
  public triangleIndicator: boolean = false;

  static get styles() {
    return css`
      .card {
        background-color: white;
        box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
        position: relative;
        height:100%
      }
      .card-small {
        width: 320px;
      }
      .card-separator {
        height: 8px;
        width: 100%;
      }

      .triangle-indicator {
        position: absolute;
        bottom: 0;
        right: -16px;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 12px 0 12px 16px;
        border-color: transparent transparent transparent #ffffff;
      }
    `;
  }

  render() {
    return html`
      <div class="card ${this.small ? 'card-small' : ''}">
        ${this.indicatorPosition === 'top'
          ? html`
              <div
                class="card-separator"
                style="background-color: ${this.indicatorColor};"
              ></div>
              <div class="card-content" style="padding: 16px;">
                <slot></slot>
              </div>
            `
          : html``}
        ${this.indicatorPosition === 'bottom'
          ? html`
          <div class="card-content" style="padding-top: 8px; padding-bottom:8px; padding-left:5px;">
            <slot></slot>
          </div>
              <div
                class="card-separator"
                style="background-color: ${this.indicatorColor}"
              ></div>
            `
          : html``}
      </div>
      ${this.triangleIndicator
        ? html`
            <div class="triangle-indicator"></div>
          `
        : html``}
    `;
  }
}
