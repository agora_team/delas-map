import { LitElement, html, customElement, property, css } from 'lit-element';
import { sharedStyles } from '../../shared-styles';

@customElement('custom-checkbox')
export class CustomCheckbox extends LitElement {
  @property({ type: Boolean })
  checked: boolean = true;

  @property({ type: String })
  checkedImage: string;

  @property({ type: String })
  uncheckedImage: string;

  @property({ type: String })
  label: string;

  static get styles() {
    return css`
      .container {
        display: flex;
        align-items: center;
      }

      .checkbox {
        display: none;
      }

      .checkbox + label {
        background-position-y: center;
        background-size: 100%;
        height: 16px;
        width: 16px;
        display: inline-block;
        padding: 0 0 1px 0px;
        margin-right: 8px;
      }

      .checkbox:checked + label {
        background-position-y: center;
        background-size: 100%;
        height: 16px;
        width: 16px;
        display: inline-block;
        padding: 0 0 1px 0px;
        margin-right: 8px;
      }
    `;
  }

  render() {
    return html`
      ${sharedStyles}
      <style>
        .checkbox + label {
          background: url(${this.uncheckedImage}) no-repeat;
        }

        .checkbox:checked + label {
          background: url(${this.checkedImage}) no-repeat;
        }
      </style>

      <div class="container">
        <input
          id="checkbox"
          class="checkbox"
          type="checkbox"
          checked=${this.checked}
          @click=${e => this.checkboxClicked()}
        />
        <label for="checkbox"> </label>
        <label for="checkbox"> ${this.label} </label>
      </div>
    `;
  }

  checkboxClicked() {
    this.checked = !this.checked;
    this.dispatchEvent(
      new CustomEvent('value-changed', {
        detail: { value: this.checked }
      })
    );
  }
}
