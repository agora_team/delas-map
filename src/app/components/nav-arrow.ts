import { LitElement, html, customElement, property } from 'lit-element';

@customElement('nav-arrow')
export class NavArrow extends LitElement {
  @property({ type: String })
  direction: 'up' | 'down' = 'up';

  @property({ type: String })
  href: string;

  images() {
    return {
      up: 'assets/img/up.png',
      down: 'assets/img/down.png'
    };
  }

  render() {
    return html`
      <style>
        .arrow {
        }
        .arrow img {
          height: 32px;
          width: 32px;
        }
        .arrow-up {
          top: 24px;
        }
        .arrow-down {
          bottom: 24px;
        }
      </style>

      <a href=${this.href} class="arrow arrow-${this.direction}">
        <img src="${this.images()[this.direction]}" />
      </a>
    `;
  }
}
