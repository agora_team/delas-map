import { html, customElement, property } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';

import { sharedStyles } from '../../../shared-styles';
import { translate, get } from '@appnest/lit-translate';
import { Info } from './info';
import { MarineWall } from '../../types';

@customElement('marine-info')
export class MarineInfo extends Info {
  @property({ type: Object })
  public wall: MarineWall;

  @property({ type: String })
  public color: string = '#367e8d';

  render() {
    return html`
      ${sharedStyles}

      <div class="card">
        ${super.renderYear()} ${super.renderTitle()}

        <span>${unsafeHTML(get(this.wall.subtitle))}</span>

        <br /><br />
        <span class="gray">
          ${unsafeHTML(get('map.card.terrestrial.geoInfo'))}
        </span>
        <br />
        <span>
          ${unsafeHTML(get(this.wall.geoInfo))}
        </span>
        ${this.renderDescription()}
      </div>
    `;
  }
}
