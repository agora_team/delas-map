import { html, customElement, property } from 'lit-element';
import { Info } from './info';

@customElement('card-info')
export class CardInfo extends Info {

  @property({ type: String })
  public color: string = 'black';

  render() {
    return html`
      <div class="card">
        ${this.renderYear()} ${this.renderTitle()} ${this.renderDescription()}
      </div>
    `;
  }
}
