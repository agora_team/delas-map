import { html, customElement, property } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';

import { sharedStyles } from '../../../shared-styles';
import { translate, get } from '@appnest/lit-translate';
import { Info } from './info';
import { TerrestrialWall } from '../../types';

@customElement('terrestrial-info')
export class TerrestrialInfo extends Info {
  @property({ type: Object })
  public wall: TerrestrialWall;

  @property({ type: String })
  public color: string = '#ca0000';

  features = {
    1: {
      lang: 'militaryItems.1',
      img: 'assets/img/EM1.png'
    },
    2: {
      lang: 'militaryItems.2',
      img: 'assets/img/EM2.png'
    },
    3: {
      lang: 'militaryItems.3',
      img: 'assets/img/EM3.png'
    },
    4: {
      lang: 'militaryItems.4',
      img: 'assets/img/EM4.png'
    },
    5: {
      lang: 'militaryItems.5',
      img: 'assets/img/EM5.png'
    },
    6: {
      lang: 'militaryItems.6',
      img: 'assets/img/EM6.png'
    },
    7: {
      lang: 'militaryItems.7',
      img: 'assets/img/EM7.png'
    },
    8: {
      lang: 'militaryItems.8',
      img: 'assets/img/EM8.png'
    },
    9: {
      lang: 'militaryItems.9',
      img: 'assets/img/EM9.png'
    },
    10: {
      lang: 'militaryItems.10',
      img: 'assets/img/EM10.png'
    },
    11: {
      lang: 'militaryItems.11',
      img: 'assets/img/EM11.png'
    },
    12: {
      lang: 'militaryItems.12',
      img: 'assets/img/EM12.png'
    },
    13: {
      lang: 'militaryItems.13',
      img: 'assets/img/EM13.png'
    },
    14: {
      lang: 'militaryItems.14',
      img: 'assets/img/EM14.png'
    },
    15: {
      lang: 'militaryItems.15',
      img: 'assets/img/EM15.png'
    }
  };

  render() {
    return html`
      ${sharedStyles}
      <style>
        .list {
          list-style-type: none;
          padding-inline-start: 0;
        }
        .frontierCountries {
          font-weight: bold;
        }
        .icon {
          padding-top:0px;
          height: 19px;
          min-width: 20px;
          padding-right: 5px;
          display: flex;
          text-align: center;
          vertical-align: middle;
          align-items: center;
          justify-content: center;
        }
        .list > li {
          display: inline-flex;
        }
        ul.list {
          margin-top: 0;
        }
        .military-icon {
          margin: auto 0;
        }
      </style>

      <div class="card">
        ${super.renderYear()} ${super.renderTitle()}

        <span class="frontierCountries">
          ${unsafeHTML(get(this.wall.frontierCountries))}
        </span>

        ${this.renderDescription()}

        <br /><br />
        <span>
          ${unsafeHTML(get(this.wall.frontierSize))}
        </span>
        <br /><br />
        <span class="gray">
          ${unsafeHTML(get('map.card.terrestrial.construction'))}
        </span>
        <br />
        <span>
          ${unsafeHTML(get(this.wall.construction))}
        </span>
        <br /><br />
        <span class="gray">
          ${unsafeHTML(get('map.card.terrestrial.geoInfo'))}
        </span>
        <br />
        <span>
          ${unsafeHTML(get(this.wall.geoInfo))}
        </span>

        <br /><br />
        <span class="gray">
          ${unsafeHTML(get('map.card.terrestrial.militaryStatus'))}
        </span>

        <ul class="list">
          ${this.wall.militaryFeatures.map(
            p => html`
              <li>
                <div class="icon">
                  <img src=${this.features[p].img} />
                </div>
                <span class="military-icon">
                  ${unsafeHTML(get(this.features[p].lang))}
                </span>
              </li>
              <br />
            `
          )}
        </ul>
      </div>
    `;
  }
}
