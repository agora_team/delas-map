import { LitElement, html, css, customElement, property } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { translate, get } from '@appnest/lit-translate';
import { Wall } from '../../types';

export class Info extends LitElement {
  @property({ type: Object })
  public wall: Wall;

  @property({ type: String })
  public color: string = '#000000';

  static get styles() {
    return css`
      .gray {
        color: gray;
      }
      .title {
        font-weight: bold;
        font-size: 13px;
      }
      .card {
        margin: 10px 20px 10px 10px;
      }
      
      .description {
        font-size: 12px;
      }
      @media screen and (max-width: 1024px) {
        
        .title{
          font-size:13px;
        }

        .description{
          font-size: 12px;
        }
        
        }
        
      @media screen and (max-width: 950px) {
        .title{
          font-size:13px;
        }

        .description{
          font-size: 11px;
        }
        
      }
        
        
      /*@media screen and (max-width: 650px) {
        .title{
          font-size:12px;
        }

        .description{
          font-size: 11px;
        }
      
      }*/
    `;
  }

  renderYear() {
    let startYear = this.yearFromTimestamp(this.wall.startDate);
    let endYear = this.yearFromTimestamp(this.wall.endDate);

    let endYearString =  (startYear !== endYear &&
      ' - ' + (endYear ? endYear : get('map.card.current')));
    return html`
      <span class="gray">
        ${unsafeHTML(
          startYear + (endYearString ? endYearString: "")
        )}
      </span>
      <br />
    `;
  }

  renderTitle() {
    return html`
      <span class="title" style="color: ${this.color}">
        ${unsafeHTML(get(this.wall.name).toUpperCase())}
      </span>
    `;
  }

  renderDescription() {
    return html`
      ${this.wall.description.map(
        des => html`
          <p class="description not-justified">${unsafeHTML(get(des))}</p>
        `
      )}
    `;
  }

  yearFromTimestamp(date) {
    if (!date) return null;
    return new Date(date).getFullYear().toString();
  }
}
