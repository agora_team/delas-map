import { html, customElement, property } from 'lit-element';
import { sharedStyles } from '../../../shared-styles';
import { Info } from './info';

@customElement('timeline-info')
export class TimelineInfo extends Info {
  @property({ type: String })
  public color: string = 'black';

  render() {
    return html`
      ${sharedStyles}

      <div class="card">
        ${this.renderYear()} ${this.renderTitle()} ${this.renderDescription()}
      </div>
    `;
  }
}
