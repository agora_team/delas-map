import { LitElement, html, customElement, property, css } from 'lit-element';
import { VirtualWall, Wall } from '../types';
import { translate } from '@appnest/lit-translate';

import './card-templates/timeline-info';
import { sharedStyles } from '../../shared-styles';

@customElement('event-timeline')
export class EventTimeline extends LitElement {
  @property({ type: Array })
  public events: Array<Wall> = [];

  @property({ type: Number })
  public startDate: number = 315532800000;

  @property({ type: Number })
  public endDate: number = new Date(2019, 1, 1).valueOf();

  @property({ type: Number })
  public pixelsPerYear: number = 160;

  @property({ type: Number })
  public currentTime: number = 0;

  @property({ type: Boolean })
  public dark: boolean = false;

  @property({ type: String })
  public backgroundColor: string;

  static get styles() {
    return css`
      .timeline-container {
        position: relative;
      }

      .virtual-wall {
        word-break: break-word;
        position: absolute;
        right: 0;
      }
    `;
  }

  render() {
    return html`
      ${sharedStyles}
      <style>
        .virtual-wall {
          color: ${this.dark ? 'white' : 'black'};
          background-color: ${this.backgroundColor};
        }
      </style>

      <div
        class="timeline-container"
        style="height: ${this.getMaxHeight()}px; top: -${this.timeToHeight(
          this.currentTime
        )}px;"
      >
        ${this.events.map(
          event => {return html`
            <div
              class="virtual-wall overlay-column"
              style="top: ${this.timeToHeight(event.internalDate)}px;"
            >
              <timeline-info
                .color=${this.dark ? 'white' : 'black'}
                .wall=${event}
              ></timeline-info>
            </div>
          `}
        )}
      </div>
    `;
  }

  getMaxHeight() {
    return (
      this.pixelsPerYear *
      (new Date(this.endDate).getFullYear() -
        new Date(this.startDate).getFullYear())
    );
  }

  timeToHeight(time: number) {
    return (
      (this.getMaxHeight() *
        (new Date(time).getFullYear() -
          new Date(this.startDate).getFullYear())) /
      (new Date(this.endDate).getFullYear() -
        new Date(this.startDate).getFullYear())
    );
  }
}
