import { Control } from 'ol/control';

import './timeline-slider';

export const TimelineControl = (function(Control) {
  function TimelineControl(
    shadowRoot,
    setTimeCallback,
    options: any = { target: null }
  ) {
    const map = shadowRoot.getElementById('ol-map');

    // Instantiate slider
    const timelineSlider = document.createElement('timeline-slider');

    // Pass all received options to the timeslider
    for (const key of Object.keys(options)) {
      timelineSlider[key] = options[key];
    }

    // Listen for the change time event
    timelineSlider.addEventListener('time-updated', (e: CustomEvent) => {
      setTimeCallback(e.detail.time);
      }
    );

    map.appendChild(timelineSlider);

    Control.call(this, { element: timelineSlider, target: options.target });
  }

  if (Control) TimelineControl.__proto__ = Control;
  TimelineControl.prototype = Object.create(Control && Control.prototype);
  TimelineControl.prototype.constructor = TimelineControl;

  return TimelineControl;
})(Control);
