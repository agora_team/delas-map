import { Control } from 'ol/control';
import { Overlay } from 'ol';

import './category-checkbox';

export function addCategoryControl(
  container,
  map,
  objectsToHide,
  category,
  filterColor
) {
  return new CategoryControl({
    container: container,
    category: category,
    color: filterColor,
    objectsToHide: objectsToHide,
    map: map
  });
}

export const CategoryControl = (function(Control) {
  function CategoryControl(options) {
    // Options
    const category = options.category;
    const color = options.color;
    const objectsToHide = options.objectsToHide;
    const mapInstance = options.map;
    const checked = options.checked || true;

    let categoryControlContainer = options.container.getElementById(
      'category-control-container'
    );

    const map = options.container.getElementById('ol-map');
    if (!categoryControlContainer) {
      categoryControlContainer = document.createElement('div');
      categoryControlContainer.setAttribute('id', 'category-control-container');
      
      if((window as any).ismobile) {
        categoryControlContainer.setAttribute(
          'style',
          'position: absolute; right: 0; top: 0; display: flex; flex-direction: column;'
        );
      }
      else {
        categoryControlContainer.setAttribute(
          'style',
          'position: absolute; right: 0; top: 0; display: flex; flex-direction: row;'
        );
      }

      map.appendChild(categoryControlContainer);
      Control.call(this, {
        element: categoryControlContainer,
        target: options.target
      });
      mapInstance.addControl(this);
    }

    const categoryControl = document.createElement('category-checkbox');
    categoryControl.className = 'overlay-column';
    categoryControl.setAttribute('category', category);
    categoryControl.setAttribute('color', color);
    categoryControl.setAttribute('checked', checked);

    categoryControlContainer.appendChild(categoryControl);

    function setVisible(object, visible: boolean) {
      if (object.values_.element) {
        // object is an overlay
        object.setPosition(visible ? [50, 50] : null);
      } else {
        object.setVisible(visible);
      }
    }

    categoryControl.addEventListener('category-checked', (e: CustomEvent) => {
      objectsToHide.forEach(o => setVisible(o, e.detail.checked));
    });

    setTimeout(() => objectsToHide.forEach(o => setVisible(o, checked)));
  }

  if (Control) CategoryControl.__proto__ = Control;
  CategoryControl.prototype = Object.create(Control && Control.prototype);
  CategoryControl.prototype.constructor = CategoryControl;

  return CategoryControl;
})(Control);
