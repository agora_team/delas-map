import { LitElement, html, customElement, property, css } from 'lit-element';
import { translate } from '@appnest/lit-translate';

import '../../card-tile';
import '../../custom-checkbox';
import { Categories } from '../../../types';

@customElement('category-checkbox')
export class CategoryCheckbox extends LitElement {
  @property({ type: String })
  category: string;

  @property({ type: String })
  color: string;

  @property({ type: Boolean })
  checked: boolean = true;

  checkedImages = {
    [Categories.marine]: 'assets/img/filtreMM.png',
    [Categories.terrestrial]: 'assets/img/filtreMT.png',
    [Categories.schengen]: 'assets/img/filtreES.png',
    [Categories.virtual]: 'assets/img/filtreMV.png',
    [Categories.frontex]: 'assets/img/filtreMMe.png'
  };

  render() {
    return html`
      <card-tile
        indicatorPosition="bottom"
        indicatorColor=${this.color}
        class="category-label"
        style="height: 50px;"
      >
        <custom-checkbox
          checkedImage=${this.checkedImages[this.category]}
          uncheckedImage="assets/img/filtre.png"
          @value-changed=${e => this.categoryClicked(e.detail.value)}
          label=${translate(this.category)}
        ></custom-checkbox>
      </card-tile>
    `;
  }

  categoryClicked(checked: boolean) {
    this.dispatchEvent(
      new CustomEvent('category-checked', {
        detail: {
          checked: checked
        }
      })
    );
  }
}
