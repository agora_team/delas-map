import {
  LitElement,
  html,
  customElement,
  PropertyValues,
  property
} from 'lit-element';

import './information-tiles';
import './app/components/ol-map';
import './land-page';
import {
  Layer,
  Categories,
  MarineWall,
  TerrestrialWall,
  VirtualWall,
  FrontexWall,
  GeoWall
} from './app/types';
import {
  use,
  get,
  registerTranslateConfig,
  extract,
  Values,
  ValuesCallback,
  ITranslateConfig,
  Key
} from '@appnest/lit-translate';

import './loading-spinner';

@customElement('my-app')
export class MyApp extends LitElement {
  layers: Layer[];

  yearsInfo: { [key: number]: string } = {};

  schengenInfo: { [key: string]: number } = {};

  fetchOptions: RequestInit = {
    headers: {
      'Content-Type': 'application/json'
    },
    mode: 'cors'
  };

  // Defer the first update of the component until the strings has been loaded to avoid empty strings being shown
  @property({ type: Boolean })
  hasLoaded = false;

  initLayers(): Layer[] {
    return [
      this.newTerrestrialLayer(),
      this.newMarineLayer(),
      this.newFrontexLayer(),
      this.newVirtualLayer(),
      this.newShengenLayer()
    ];
  }

  newFrontexLayer() {
    return <Layer<FrontexWall>>{
      category: Categories.frontex,
      color: '#000000',
      dark: true,
      image: 'assets/img/F.png',
      frontexImage: 'assets/img/F.png',
      geoWalls: [],
      timeWalls: []
    };
  }

  newVirtualLayer() {
    return <Layer<VirtualWall, VirtualWall>>{
      category: Categories.virtual,
      color: '#5f5146',
      dark: true,
      image: 'assets/img/MV.png',
      frontexImage: 'assets/img/MV.png',
      geoWalls: [],
      timeWalls: []
    };
  }

  newVirtualWall(info) {
    return {
      startDate: new Date(info.year, 1, 1).valueOf(),
      endDate: new Date(info.year, 1, 1).valueOf(),
      name: info.name,
      internalDate: new Date(info.internalDate, 1, 1).valueOf(),
      description: [info.description1, info.description2]
    };
  }

  newShengenLayer() {
    return <Layer<FrontexWall>>{
      category: Categories.schengen,
      color: '#cecece',
      image: 'assets/img/F.png',
      frontexImage: 'assets/img/F.png',
      geoWalls: [],
      timeWalls: []
    };
  }

  newShengenWall(info) {
    return {
      startDate: new Date(info.year, 1, 1).valueOf(),
      endDate: null,
      name: info.country,
      internalDate: new Date(info.internalDate, 1, 1).valueOf(),
      description: [info.text]
    };
  }

  newTerrestrialLayer() {
    return <Layer<GeoWall>>{
      category: Categories.terrestrial,
      color: '#ca0000',
      image: 'assets/img/MT.png',
      frontexImage: 'assets/img/MTF.png',
      disableFilter: (window as any).ismobile,
      geoWalls: [],
      timeWalls: []
    };
  }

  newTerrestrialWall(info) {
    let militaryFeatures = [];

    for (let i = 1; i < 16; i++) {
      if (info[i] == 'si') {
        militaryFeatures.push(i);
      }
    }

    return {
      latitude: info.latitude,
      longitude: info.longitude,
      frontex: info.frontex.toLowerCase() == 'si',
      startDate: new Date(info.startDate, 1, 1).valueOf(),
      endDate: new Date(info.endDate, 1, 1).valueOf(),
      name: info.name,
      frontierCountries: info.frontierCountries,
      description: [info.description],
      frontierSize: info.frontierSize,
      construction: info.construction,
      geoInfo: info.geoInfo,
      militaryFeatures: militaryFeatures
    };
  }

  newMarineLayer() {
    return <Layer<MarineWall>>{
      category: Categories.marine,
      color: '#367e8d',
      image: 'assets/img/MM.png',
      frontexImage: 'assets/img/MMF.png',
      disableFilter: (window as any).ismobile,
      geoWalls: [],
      timeWalls: []
    };
  }

  newMarineWall(info) {
    return {
      latitude: info.latitude,
      longitude: info.longitude,
      name: info.name,
      frontex: info.frontex.toLowerCase() == 'si',
      startDate: new Date(info.startDate, 1, 1).valueOf(),
      endDate: info.endDate ? new Date(info.endDate, 1, 1).valueOf() : null,
      description: [info.description],
      geoInfo: info.geoInfo,
      subtitle: ''
    };
  }

  addFixedWalls() {
    this.layers.push(<Layer<FrontexWall>>{
      category: Categories.frontex,
      color: '#000000',
      image: 'assets/img/F.png',
      frontexImage: 'assets/img/F.png',
      disableFilter: true,
      geoWalls: [
        {
          latitude: 52,
          longitude: 21,
          name: get('EUFrontex.name'),
          startDate: new Date(2004, 1, 1).valueOf(),
          endDate: new Date(2004, 1, 1).valueOf(),
          description: [
            get('EUFrontex.description1'),
            get('EUFrontex.description2')
          ]
        }
      ],
      timeWalls: []
    });

    this.layers[2].geoWalls.push({
      latitude: 59,
      longitude: 24,
      name: get('EULISA.name'),
      startDate: new Date(2011, 1, 1).valueOf(),
      endDate: new Date(2011, 1, 1).valueOf(),
      description: [get('EULISA.description1'), get('EULISA.description2')]
    });
  }

  fetchData(lang, filename, key, layer, wallFunction): Promise<void> {
    return fetch(
      'assets/data/' + filename + '_' + lang + '.json',
      this.fetchOptions
    )
      .then(res => {
        return res.json();
      })
      .then(info => {
        layer = info.reduce((obj, info) => {
          obj[key].push(wallFunction(info));
          return obj;
        }, layer);
      })
      .catch(error => console.log(error));
  }

  // Load the initial language and mark that the strings has been loaded.
  formatString(str): string {
    return str
      .replace(/(\B)[^ ]*/g, match => match.toLowerCase())
      .replace(/^[^ ]/g, match => match.toUpperCase());
  }

  connectedCallback() {
    super.connectedCallback();
    registerTranslateConfig({
      loader: lang => {
        this.layers = this.initLayers();

        return Promise.all([
          fetch('assets/locales/' + lang + '.json', this.fetchOptions).then(
            res => res.json()
          ),
          fetch('assets/data/years_info_' + lang + '.json', this.fetchOptions)
            .then(res => res.json())
            .then(info => (this.yearsInfo = info)),
          fetch('assets/data/shengen_info.json', this.fetchOptions)
            .then(res => res.json())
            .then(
              info =>
                (this.schengenInfo = Object.keys(info).reduce(
                  (obj, key) => ({
                    ...obj,
                    [key]: new Date(info[key], 1, 1).valueOf()
                  }),
                  {}
                ))
            ),
          this.fetchData(
            lang,
            'terrestrial_info',
            'geoWalls',
            this.layers[0],
            this.newTerrestrialWall
          ),
          this.fetchData(
            lang,
            'marine_info',
            'geoWalls',
            this.layers[1],
            this.newMarineWall
          ),
          this.fetchData(
            lang,
            'frontex',
            'timeWalls',
            this.layers[2],
            this.newShengenWall
          ),
          this.fetchData(
            lang,
            'virtuals_info',
            'timeWalls',
            this.layers[3],
            this.newVirtualWall
          ),
          this.fetchData(
            lang,
            'schengen_timeline',
            'timeWalls',
            this.layers[4],
            this.newShengenWall
          ),
        ]).then(response => response[0]);
      },
      empty: (key: Key, config: ITranslateConfig) => `${key}`
    });

    let lang = localStorage.getItem('lang') || 'es';
    use(lang).then(() => {
      document.title = this.formatString(get('landpage.title'));
      this.addFixedWalls();
      this.hasLoaded = true;
    });
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      <style>
        .center {
          position: fixed;
          left: 50%;
          top: 50%;
          transform: translate(-50%, -50%);
        }
      </style>
      ${this.hasLoaded
        ? html`
            <section>
              <a id="landpage"></a>
              <land-page></land-page>
            </section>

            <section>
              <a id="information"></a>

              <information-tiles></information-tiles>
            </section>

            <section>
              <a id="map"></a>
              <ol-map
                .layers=${this.layers}
                .yearsInfo=${this.yearsInfo}
                .schengenInfo=${this.schengenInfo}
              ></ol-map>
            </section>
          `
        : html`
            <loading-spinner class="center"></loading-spinner>
          `}
    `;
  }
}
