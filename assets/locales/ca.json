{
    "marine": "Muros marítims",
    "terrestrial": "Murs terrestres",
    "virtual": "Murs virtuales",
    "schengen": "Espai Schengen",
    "frontex": "Frontex",
    "landpage": {
      "title": "AIXECANT MURS",
      "subtitle": "L'Europa Fortalesa",
      "description": "Arreu del món s'estan erigint <strong>edificis de la por</strong>, tant reals com imaginaris, que provoquen l'augment de la xenofòbia i creen un <strong>món emmurallat molt més perillós</strong> per a totes aquelles persones que busquen refugi i seguretat. Els estats membres de la Unió Europea i l'espai Schengen han alçat prop de <strong>1000 Km de murs</strong>, l'equivalent a més de sis murs de Berlín. Aquests murs van acompanyats dels encara més llargs <strong>murs marítims</strong> desplegats al mar Mediterrani,  així com els <strong>murs virtuals</strong>, sistemes de control fronterer que busquen parar a les persones que intenten entrar a Europa, constituint així l'anomenada <strong>Europa Fortalesa</strong>",
      "share": "Compartir",
      "delasalticon" : "CENTRO DELÁS DE ESTUDIOS POR LA PAZ",
	"colaboration_of": "Amb el suport de:"
    },
    "information": {
      "title": "Polítiques de  la por i securitització a la Unió Europea",
      "quote1":"<i>\"Amb la greu amenaça terrorista que pesa sobre nosaltres, hem de poder controlar qui entra per poder expulsar a qui representa un perill\"</i> (Marine Le Pen)",
      "quote2": "<i>\"No és suficient reduir el nombre de persones que arriben. Necessitem augmentar les deportacions\"</i>(Matteo Salvini).",
      "description": "La Unió Europea fa servir una <strong>retòrica i una narrativa contradictòries</strong> pel que fa als movimients migratoris. Mentre es rebutja la política de murs (Nielsen, 2017) i s'anima a donar suport a la perspectiva humanitària i de cooperació, es promou un discurs i unes pràctiques securitàries que <strong>criminalitzen el moviment de les persones que migren</strong>, concebent-les com una amenaça.",
      "description2": "<small><i></br>*Font: Informe 35. Aixecant Murs. Polítiques de  la por i securitització a la Unió Europea. publicat pel Centre Delàs, el Transnational Institute i Stop Wapenhandel.</i></small>",
      "schengen": {
        "title": "Espai Schengen",
        "description": "Es crea amb l' <strong>Acord de Schengen</strong> signat l'any <strong>1985</strong> per 5 Estats: Alemanya, França, Bèlgica, Luxemburg i Països Baixos. A ell s'aniran subscrivint d'altres Estats fins a arribar a l'àrea actual constituïda per un total de 26.</br>Unir-se a l'espai Schengen no és simplement una decisió política, els països també han de complir una llista de condicions prèvies que inclouen la seva <strong>capacitat per al control efectiu de les seves fronteres</strong>.</br>L'acord ha tingut diverses fites importants: L'any 1990 s'aprova la Schengen implementing Convention (SIC) que concedeix el dret a lliure moviment entre població dels països de l'Espai  <strong>traslladant els controls a les fronteres externes del mateix</strong>. No obstant això, el 2006 amb l'establiment del Schengen Borders Code es regula la <strong>reintroducció temporal de controls interns en les fronteres</strong> dels Estats membres. Així mateix, el 2016 s'aprova la implementació del Plan on Security and Defence on es menciona la necessitat d' <strong>enfortir les fronteres en tercers països</strong>."
      },
      "frontex": {
        "title": "Agència Europea de la Guardia de Fronteres i Costes (FRONTEX)",
        "description": "La seva principal funció és la de controlar els crims relacionats amb els espais fronterers, el que inclou interceptar a les persones refugiades i migrades perquè no arribin a la vora del mar dels estats membres. En 2016 s'amplia el mandat de FRONTEX i passa a coordinar les operacions de cooperació al retorn posant-se a la seva disposició un cos d'agents de guàrdia de fronteres propi. El seu pressupost s'ha disparat passant de 6,2 milions d'euros en l'any 2005 als 302 milions l'any 2017. <strong>FRONTEX NO és per tant una Agència de rescat de persones sinó de vigilància i control de fronteres</strong>."
      },
      "mental_walls" : {
        "title": "Murs Mentals",
        "description": "Fan referència als <strong> sistemes de vigilància i seguretat basats en sistemes tecnològics </strong> per al tractament i gestió dels fluxos migratoris i del moviment de persones o el que ha estat anomenat <strong> \"la tecnologització de la seguretat \"</strong>. Normalment acompanyen i reforcen les barreres físiques i les funcions dels agents de fronteres. La detecció de dades biomètriques és una de les seves principals característiques. L'anàlisi d'aquests sistemes en el present mapa es centra en aquells desenvolupats per la UE, sense fer èmfasi en la seva implementació en territoris o països específics. Dins dels murs virtuals es destaca el paper de <strong> l'Agència Eu-Lisa </strong>, clau en la gestió operativa de sistemes informàtics relacionats amb el control de fronteres."
      },
      "terrestrial_walls" : {
        "title": "Murs Terrestres",
        "description": "Són aquells <strong>murs i tanques terrestres</strong> que múltiples Estats de la UE i de l'espai Schengen estan aixecant, com a <strong>estratègia de control i gestió de fronteres</strong> des dels anys 90 fins a l'actualitat. Aquests murs produeixen el desviament de persones que es veuen obligades a buscar noves rutes menys segures, exposant les seves vides a un major risc. Aquests murs <strong>no només es componen de materials</strong>  com filferros espinosos, pues i formigó sinó que <strong>també inclouen sistemes de vigilància</strong>  com: càmeres, drons, sensors òptics i acústics, lectura de passaports, alarmes, càmeres tèrmiques, detectors de moviment i personal de seguretat com guàrdies i soldats."
      },
      "maritime_walls" : {
        "title": "Murs Marítims",
        "description": "Fan referència a les principals <strong>operaciones marítimes</strong> realitzades per al <strong>control dels fluxos migratoris</strong>, les quals en la seva gran majoria es troben liderades per <strong>FRONTEX</strong>. L'estudi recull les principals operacions realitzades al Mediterrani d'acord amb la seva duració en el temps i desplegament.</br> De les 8 principals operacions marítimes de la UE <strong>cap té un mandat exclusiu de rescat de persones</strong> tret de l'Operació Mare Nostrum liderada pel govern italià, posteriorment substituïda per l'Operació Tritó, operació ja desenvolupada de manera conjunta amb FRONTEX."
      },
      "virtual_walls" : {
        "title": "Murs Virtuals",
        "description": "Es refereixen als <strong>sistemes de vigilància i seguretat basats en sistemes tecnològics</strong> per al tractament i gestió dels fluxos migratoris i del moviment de persones o el que ha estat anomenat com <strong>\"la tecnologització de la seguretat\"</strong>. Normalment acompanyen i reforcen les barreres físiques i les funcions dels agents de fronteres. La detecció de dades biomètriques serà una de les característiques d'aquests sistemes. L'anàlisi d'aquests sistemes en el present mapa es centra en els sistemes desenvolupats per la UE, sense posar èmfasi en la seva implementació en territoris o països específics. Dins dels murs virtuals es destaca el paper de l' <strong>Agència Eu-Lisa</strong> clau en la gestió operativa de sistemes informàtics relacionats amb el control de fronteres."
      }
    },
    "map": {
      "card": {
        "current": "Actualitat",
        "terrestrial": {
          "construction": "Raons per a la seva construcció:",
          "geoInfo": "Delimitació geogràfica que pretén:",
          "militaryStatus": "Estat de militarització:￼"
        },
        "marine": {
          "geoInfo": "Zona geogràfica d'actuació:"
        }
      }
    },
    "militaryItems": {
      "1": "Mur de formigó",
      "2": "Tanca de filferro",
      "3": "Tanca de filferro electrificada",
      "4": "Filferro espinós",
      "5": "Soldats",
      "6": "Guàrdies de fronteres",
      "7": "Control de pasaports",
      "8": "Cotxes tot terreny",
      "9": "Càmeres i sistemes de videovigilància",
      "10": "Sensors/càmeres tèrmiques",
      "11": "Detecció de diòxid de carboni en camions",
      "12": "Sistemes electrònics de vigilància o sensors de moviment",
      "13": "Torre de control",
      "14": "Drons",
      "15": "Alarma"
    },
    "yearsInfo": {
      "1980": "Sistemes electrònics de vigilància o sensors de moviment"
    },
    "EUFrontex" : {
      "name" : "Agència Europea de la Guàrdia de Fronteres i Costes (FRONTEX)",
      "description1" : "Es crea a Varsòvia al 2004 però no comença a funcionar fins al 2005.",
      "description2" : "Opera com a encarregada de la gestió de la Cooperació Operativa en les Fronteres Exteriors dels estats membres de la UE."
    },
    "EULISA" : {
      "name" : "Agència Europea per a la Gestió Operativa de Sistemes Informàtics de Gran Magnitud en l’Espai de Llibertat, Seguretat i Justícia (EU-LISA)",
      "description1" : "Entra en funcionament a finals de l'any 2012. Té seu principal a Tallin i operativa a Estrasburg.",
      "decription2" : "Responsable de la gestió dels principals sistemes informàtics relacionats amb el control de fronteres i el moviment dins la U.E.: Eurodac, el Sistema d’Información Schengen de segona generació (SIS II) i el Sistema d’Informació de Visats (VIS)."
    }
  }
  